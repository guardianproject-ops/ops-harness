#!/usr/bin/env bash
export OPS_HARNESS_ORG=${1:-guardianproject-ops}
export OPS_HARNESS_PROJECT=${2:-ops-harness}
export OPS_HARNESS_BRANCH=${3:-master}
export GITLAB_REPO="https://gitlab.com/${OPS_HARNESS_ORG}/${OPS_HARNESS_PROJECT}.git"

if [ "$OPS_HARNESS_PROJECT" ] && [ -d "$OPS_HARNESS_PROJECT" ]; then
  echo "Removing existing $OPS_HARNESS_PROJECT"
  rm -rf "$OPS_HARNESS_PROJECT"
fi

echo "Cloning ${GITLAB_REPO}#${OPS_HARNESS_BRANCH}..."
git clone -c advice.detachedHead=false --depth=1 -b $OPS_HARNESS_BRANCH $GITLAB_REPO
